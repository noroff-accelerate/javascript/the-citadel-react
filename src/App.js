
/**
 * Dependencies
 * @ignore
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'

/**
 * Module Dependencies
 * @ignore
 */
import { wrapDispatch, appBootstrap } from './api'
import AuthHandler from './container/AuthHandler'

/**
 * App
 * @ignore
 */
class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      loading: true,
      error: null,
    }

    const { dispatch } = this.props
    this.appBootstrap = wrapDispatch(dispatch, appBootstrap)
  }

  static mapStateToProps ({ access_token, config }, ownProps = {}) {
    return {
      access_token,
      basename: config.env.base_path,
      ...ownProps
    }
  }

  componentDidMount () {
    // Try load application and auth server configuration
    this.asyncLoad()
      .then(state => {
        this.appBootstrap(state)
        this.setState({ loading: false })
      })
      .catch(error => {
        console.error(error)
        this.setState({ loading: false, error })
      })
  }

  loadApplicationConfiguration () {
    const env = [
      { name: 'debug_logging', env: 'REACT_APP_DEBUG_LOGGING', default: '' },
      { name: 'client_id', env: 'REACT_APP_CLIENT_ID' },
      { name: 'token_scopes', env: 'REACT_APP_TOKEN_SCOPE', default: 'openid profile email' },
      { name: 'auth_server', env: 'REACT_APP_AUTH_SERVICE' },
      { name: 'base_url', env: 'REACT_APP_BASE_URL' },
      { name: 'base_path', env: 'REACT_APP_BASE_PATH', default: '/' },
    ]

    // For missing required configuration
    const missing = []

    const config = env.reduce((state, desc) => {
      const { name, env: key, default: val } = desc

      // Set default if provided
      if (val !== undefined) {
        state[name] = val
      }

      // Load env if provided (overrides default)
      if (process.env[key]) {
        state[name] = process.env[key]
      }

      // Mark missing
      if (state[name] === undefined) {
        missing.push(desc)
      }

      return state
    }, {})

    // Throw if required config is missing
    if (missing.length) {
      const required = missing.map(m => m.env).join(', ')
      console.error(`[INIT] Missing required configuration: ${required}`)
      throw new Error(`Missing required configuration: ${required}`)
    }

    return config
  }

  async asyncLoad () {
    const env = this.loadApplicationConfiguration()
    console.log('ENV', env)

    let discovery, jwks

    // Fetch discovery document
    try {
      const response = await fetch(`${env.auth_server}/.well-known/openid-configuration`)
      discovery = await response.json()

      console.log('DISCOVERY', discovery)

    } catch (err) {
      console.error('[INIT] Failed to load discovery document')
      throw err
    }

    // Fetch jwks
    try {
      const response = await fetch(discovery.jwks_uri)
      jwks = await response.json()

      console.log('JWKS', jwks)

    } catch (err) {
      console.error('[INIT] Failed to load jwks')
      throw err
    }

    return { env, discovery, jwks }
  }

  renderChildren () {
    const { access_token } = this.props
    const { error } = this.state

    if (error) {
      return <small>Error. See Developer Console.</small>
    }

    return (
      <Route path="/" render={({ history }) => {
        if (!access_token) {
          history.push('/login')
        }

        return (
          <div className="container-fluid" style={{ marginTop: 24 }}>
            <div className="container-md">
              <div className="container-fluid">
                <p className="display-1">Access Token</p>
                <code>{access_token}</code>
              </div>
            </div>
          </div>
        )
      }} />
    )
  }

  render () {
    const { loading } = this.state
    const { basename } = this.props

    if (loading) {
      return <small>Loading</small>
    }

    return (
      <Router basename={basename}>
        <Route path="/login" component={AuthHandler} />
        {this.renderChildren()}
      </Router>
    )
  }
}

export default connect(App.mapStateToProps)(App)
