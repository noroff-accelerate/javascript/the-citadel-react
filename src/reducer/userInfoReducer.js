
const type_string = 'SET_USERINFO'

// Export type string
export { type_string as type }

/**
 * Reducer
 * @ignore
 */
export default function (state = null, action = {}) {
  const { type, userinfo } = action

  if (type === type_string && userinfo) {
    return userinfo
  }

  return state
}