
const type_string = 'SET_ACCESS_TOKEN'

// Export type string
export { type_string as type }

/**
 * Reducer
 * @ignore
 */
export default function (state = null, action = {}) {
  const { type, access_token } = action

  if (type === type_string && access_token) {
    return access_token
  }

  return state
}