
const type_string = 'SET_APPLICATION_CONFIGURATION'

// Export type string
export { type_string as type }

const placeholder = {
  env: {},
  discovery: {},
  jwks: {},
}

/**
 * Reducer
 * @ignore
 */
export default function (state = placeholder, action = {}) {
  const { type, config } = action

  if (type === type_string && config) {
    return config
  }

  return state
}