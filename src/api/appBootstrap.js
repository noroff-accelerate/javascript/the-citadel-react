
/**
 * Dependencies
 * @ignore
 */
import { bootstrapReducerType as type } from '../reducer'

/**
 * Action
 * @ignore
 */
export default function (config) {
  return { type, config }
}