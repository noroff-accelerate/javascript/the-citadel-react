
/**
 * Wrap Dispatcher
 * @ignore
 */
export function wrapDispatch (dispatch, fn) {
  return (...args) => dispatch(fn(...args))
}

/**
 * Re-exports
 * @ignore
 */
export { default as setAccessToken } from './setAccessToken'
export { default as setUserInfo } from './setUserInfo'
export { default as appBootstrap } from './appBootstrap'