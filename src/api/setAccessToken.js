
/**
 * Dependencies
 * @ignore
 */
import { accessTokenReducerType as type } from '../reducer'

/**
 * Action
 * @ignore
 */
export default function (access_token) {
  return { type, access_token }
}