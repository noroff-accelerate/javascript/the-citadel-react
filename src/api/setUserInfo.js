
/**
 * Dependencies
 * @ignore
 */
import { userInfoReducerType as type } from '../reducer'

/**
 * Action
 * @ignore
 */
export default function (userinfo) {
  return { type, userinfo }
}