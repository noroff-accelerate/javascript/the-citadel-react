
/**
 * Dependencies
 * @ignore
 */
import { createStore, combineReducers } from 'redux'

/**
 * Module Dependencies
 * @ignore
 */
import {
  accessTokenReducer as access_token,
  userInfoReducer as userinfo,
  bootstrapReducer as config,
} from './reducer'

/**
 * Store
 * @ignore
 */
const store = createStore(combineReducers({
  config,
  access_token,
  userinfo,
}))

/**
 * Exports
 * @ignore
 */
export default store
