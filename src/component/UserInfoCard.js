
/**
 * Dependencies
 * @ignore
 */
import React from 'react'

/**
 * Module Dependencies
 * @ignore
 */

/**
 * User Info Card
 * @ignore
 */
const UserInfoCard = (props) => {
  return (
    <div className="card" style={{ width: '18rem', marginBottom: 8 }}>
      <img src={props.avatar_url} className="card-img-top" alt={props.name} />
      <div className="card-body">
        <h5 className="card-title">{props.name}</h5>
        <p className="card-subtitle mb-2 text-muted">{props.username}</p>
      </div>
    </div>
  )
}

/**
 * Exports
 * @ignore
 */
export default UserInfoCard