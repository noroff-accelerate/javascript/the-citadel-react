/**
 * Dependencies
 * @ignore
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'

/**
 * Module Dependencies
 * @ignore
 */
import store from './store'
import * as serviceWorker from './serviceWorker'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css'

/**
 * Mount
 * @ignore
 */
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'))

/**
 * Deregister Service Worker
 * @ignore
 */
serviceWorker.unregister()
