
/**
 * Dependencies
 * @ignore
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'

/**
 * Module Dependencies
 * @ignore
 */
import { wrapDispatch, setAccessToken } from '../api'

/**
 * Component
 * @ignore
 */
class AuthHander extends Component {

  constructor (...args) {
    super(...args)

    const { dispatch } = this.props
    this.setAccessToken = wrapDispatch(dispatch, setAccessToken)
  }

  static mapStateToProps ({ access_token, config }, ownProps) {
    return {
      config: {
        redirect_uri: `${config.env.base_url}${config.env.base_path}/login`,
        client_id: config.env.client_id,
        scope: config.env.token_scopes,
        response_type: 'token',
      },
      authorization_endpoint: config.discovery.authorization_endpoint,
      jwks: config.jwks,
      access_token,
      ...ownProps
    }
  }

  componentDidUpdate () {
    const { history, access_token: token } = this.props

    if (token) {
      history.push('/')
    }
  }

  componentDidMount () {
    const { location, config, authorization_endpoint } = this.props

    try {
      if (location.hash) {
        const { access_token } = Object.fromEntries(new URLSearchParams(location.hash.substring(1)).entries())

        console.log('TOKEN', access_token)
        return this.setAccessToken(access_token)
      }

      // TODO Try load from local storage
      // TODO Verify token signature

      const buf = new ArrayBuffer(16)
      crypto.getRandomValues(new Uint8Array(buf))
      const nonce = btoa(String.fromCharCode(...new Uint8Array(buf)))
      window.location = `${authorization_endpoint}?${new URLSearchParams({ ...config, nonce }).toString()}`

    } catch (err) {
      console.error(err)
    }
  }

  render () {
    // null op
    return <></>
  }
}

/**
 * Exports
 * @ignore
 */
export default connect(AuthHander.mapStateToProps)(AuthHander)