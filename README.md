# The Citadel (React)

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pages](https://img.shields.io/static/v1?logo=gitlab&message=view&label=pages&color=e24329)](https://noroff-accelerate.gitlab.io/javascript/the-citadel-react)

> Frontend for The Citadel demo case

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash
npm start
```

## Build

Create a minified, production build of the project for publication.

```bash
npm run build
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
